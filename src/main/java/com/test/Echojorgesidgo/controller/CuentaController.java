package com.test.Echojorgesidgo.controller;

import com.test.Echojorgesidgo.model.dto.CuentaDTO;
import com.test.Echojorgesidgo.model.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/cuentas")
public class CuentaController {

    @Autowired
    CuentaService cuentaService;

    @GetMapping("/deposito")
    public String depositoView(
            Model model
    ) {

        model.addAttribute("message", "");
        model.addAttribute("cuenta", new CuentaDTO());

        return "deposito";
    }


    @GetMapping("/deposito/cuenta")
    public String depositoWithCuenta(
            @RequestParam("cuenta") String cuenta,
            Model model
    ) {

        model.addAttribute("message", "");
        model.addAttribute("cuenta", cuentaService.getCuenta(cuenta));

        return "deposito";
    }

    @PostMapping("/deposito")
    public String deposito(
            @ModelAttribute CuentaDTO cuenta,
            Model model
    ) {

        model.addAttribute("cuenta", cuentaService.deposito(cuenta));
        model.addAttribute("message", "Deposito realizado");

        return "deposito";
    }


}
