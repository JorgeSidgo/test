package com.test.Echojorgesidgo.model.service.impl;

import com.test.Echojorgesidgo.model.dto.CuentaDTO;
import com.test.Echojorgesidgo.model.entity.Cliente;
import com.test.Echojorgesidgo.model.entity.Cuenta;
import com.test.Echojorgesidgo.model.repository.CuentaRepository;
import com.test.Echojorgesidgo.model.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    CuentaRepository cuentaRepository;

    @Override
    public CuentaDTO deposito(BigDecimal monto) {
        return null;
    }

    @Override
    public List<CuentaDTO> crearCuentas(long idCliente, List<CuentaDTO> cuentas) {
        List<CuentaDTO> cuentaDTOList = new ArrayList<>();

        try {
            cuentas.forEach(item -> {
                Cuenta cuenta = new Cuenta();

                cuenta.setCliente(new Cliente(idCliente));
                cuenta.setMontoApertura(item.getMontoApertura());
                cuenta.setSaldo(item.getMontoApertura());
                cuenta.setTipoCuenta(item.getTipoCuenta());

                cuenta = cuentaRepository.save(cuenta);

                cuenta.setNumCuenta(String.format("%010d", cuenta.getId()));

                cuentaDTOList.add(CuentaDTO.fromEntity(cuentaRepository.save(cuenta)));
            });
        } catch (Exception ex) {
            throw ex;
        }

        return cuentaDTOList;
    }

    @Override
    public CuentaDTO agregarCuenta(long idCliente, CuentaDTO cuentaDTO) {
        try {
            Cuenta cuenta = new Cuenta();

            cuenta.setCliente(new Cliente(idCliente));
            cuenta.setMontoApertura(cuentaDTO.getMontoApertura());
            cuenta.setSaldo(cuentaDTO.getMontoApertura());
            cuenta.setTipoCuenta(cuentaDTO.getTipoCuenta());
            cuenta.getTipoCuenta();

            cuenta = cuentaRepository.save(cuenta);

            cuenta.setNumCuenta(String.format("%010d", cuenta.getId()));

            return CuentaDTO.fromEntity(cuentaRepository.save(cuenta));

        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public CuentaDTO getCuenta(String numCuenta) {
        return cuentaRepository.getCuentaByNumCuenta(numCuenta);
    }

    @Override
    public CuentaDTO deposito(CuentaDTO cuentaDTO) {
        Cuenta cuenta = new Cuenta();

        try {

            cuenta = cuentaRepository.getOne(cuentaDTO.getId());

            cuenta.setSaldo(cuenta.getSaldo().add(cuentaDTO.getMonto()));

        } catch (Exception ex) {
            throw ex;
        }

        return  CuentaDTO.fromEntity(cuentaRepository.save(cuenta));
    }

    @Override
    public List<CuentaDTO> cuentasCliente(long idCliente) {
        return cuentaRepository.getCuentasByCliente(idCliente);
    }
}
