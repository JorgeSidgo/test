package com.test.Echojorgesidgo.model.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "cuenta")
public class Cuenta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "numcuenta")
    private String numCuenta;

    @Column(name = "montoapertura")
    private BigDecimal montoApertura;

    @Column(name = "saldo")
    private BigDecimal saldo;

    @Column(name = "tipocuenta")
    private char tipoCuenta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "codcliente")
    private Cliente cliente;

    public Cuenta() {
    }

    public Cuenta(long id) {
        this.id = id;
    }

    public Cuenta(long id, String numCuenta, BigDecimal montoApertura, BigDecimal saldo, char tipoCuenta, Cliente cliente) {
        this.id = id;
        this.numCuenta = numCuenta;
        this.montoApertura = montoApertura;
        this.saldo = saldo;
        this.tipoCuenta = tipoCuenta;
        this.cliente = cliente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public BigDecimal getMontoApertura() {
        return montoApertura;
    }

    public void setMontoApertura(BigDecimal montoApertura) {
        this.montoApertura = montoApertura;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public char getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(char tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
