package com.test.Echojorgesidgo.model.repository;

import com.test.Echojorgesidgo.model.dto.CuentaDTO;
import com.test.Echojorgesidgo.model.entity.Cuenta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CuentaRepository extends BaseRepository<Cuenta, Long> {

    @Query("select new com.test.Echojorgesidgo.model.dto.CuentaDTO(c.id, c.numCuenta, c.montoApertura, c.saldo, c.tipoCuenta) from Cuenta c where c.cliente.codCliente = :id")
    List<CuentaDTO> getCuentasByCliente(@Param("id") long id);

    @Query("select new com.test.Echojorgesidgo.model.dto.CuentaDTO(c.id, c.numCuenta, c.montoApertura, c.saldo, c.tipoCuenta, c.cliente) from Cuenta c where c.numCuenta = :num")
    CuentaDTO getCuentaByNumCuenta(@Param("num") String num);

    @Query("select c from Cuenta c where c.id = :id")
    Cuenta getCuentaById(@Param("id") long id);
}
