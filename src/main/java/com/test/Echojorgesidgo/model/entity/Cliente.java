package com.test.Echojorgesidgo.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codcliente")
    private long codCliente;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "DUI")
    private String dui;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.REMOVE)
    private List<Cuenta> cuentas;

    public Cliente() {
    }

    public Cliente(long codCliente) {
        this.codCliente = codCliente;
    }

    public Cliente(long codCliente, String nombre, String apellido, String dui) {
        this.codCliente = codCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dui = dui;
    }

    public long getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(long codCliente) {
        this.codCliente = codCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }
}

