package com.test.Echojorgesidgo.model.dto;

import com.test.Echojorgesidgo.model.entity.Cliente;
import com.test.Echojorgesidgo.model.entity.Cuenta;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CuentaDTO {

    private long id;
    private String numCuenta;
    @NotNull(message = "Ingrese el monto de apertura")
    private BigDecimal montoApertura;
    private BigDecimal saldo;
    private BigDecimal monto;
    private char tipoCuenta;
    private Cliente cliente;

    public CuentaDTO() {
    }

    public CuentaDTO(long id) {
        this.id = id;
    }

    public CuentaDTO(long id, String numCuenta, BigDecimal montoApertura, BigDecimal saldo, char tipoCuenta) {
        this.id = id;
        this.numCuenta = numCuenta;
        this.montoApertura = montoApertura;
        this.saldo = saldo;
        this.tipoCuenta = tipoCuenta;
    }

    public CuentaDTO(long id, String numCuenta, @NotNull(message = "Ingrese el monto de apertura") BigDecimal montoApertura, BigDecimal saldo, char tipoCuenta, Cliente cliente) {
        this.id = id;
        this.numCuenta = numCuenta;
        this.montoApertura = montoApertura;
        this.saldo = saldo;
        this.tipoCuenta = tipoCuenta;
        this.cliente = cliente;
    }

    public static CuentaDTO fromEntity(Cuenta cuenta) {
        return new CuentaDTO(
                cuenta.getId(),
                cuenta.getNumCuenta(),
                cuenta.getMontoApertura(),
                cuenta.getSaldo(),
                cuenta.getTipoCuenta()
        );
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public BigDecimal getMontoApertura() {
        return montoApertura;
    }

    public void setMontoApertura(BigDecimal montoApertura) {
        this.montoApertura = montoApertura;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public char getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(char tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }
}
