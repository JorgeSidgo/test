package com.test.Echojorgesidgo.model.dto;

import com.test.Echojorgesidgo.model.entity.Cliente;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class ClienteDTO {

    private long codCliente;
    @NotEmpty(message = "Ingrese los nombres")
    private String nombre;
    @NotEmpty(message = "Ingrese los apellidos")
    private String apellido;
    @NotEmpty(message = "Ingrese el DUI")
    private String dui;
    private List<CuentaDTO> cuentas;

    public ClienteDTO() {
    }

    public ClienteDTO(long codCliente) {
        this.codCliente = codCliente;
    }

    public ClienteDTO(long codCliente, String nombre, String apellido, String dui) {
        this.codCliente = codCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dui = dui;
    }

    public ClienteDTO(long codCliente, String nombre, String apellido, String dui, List<CuentaDTO> cuentas) {
        this.codCliente = codCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dui = dui;
        this.cuentas = cuentas;
    }

    public static ClienteDTO fromEntity(Cliente cliente) {
        return new ClienteDTO(
                cliente.getCodCliente(),
                cliente.getNombre(),
                cliente.getApellido(),
                cliente.getDui()
        );
    }

    public long getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(long codCliente) {
        this.codCliente = codCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public List<CuentaDTO> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<CuentaDTO> cuentas) {
        this.cuentas = cuentas;
    }
}
