package com.test.Echojorgesidgo.model.service;

import com.test.Echojorgesidgo.exception.ExistingClientException;
import com.test.Echojorgesidgo.model.dto.ClienteDTO;

import java.util.List;

public interface ClienteService {

    List<ClienteDTO> list();
    ClienteDTO save(ClienteDTO clienteDTO) throws ExistingClientException;
    ClienteDTO update(long id,ClienteDTO clienteDTO) throws ExistingClientException;
    ClienteDTO getOne(long id);
    void delete(long idCliente);

}
