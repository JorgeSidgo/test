package com.test.Echojorgesidgo.controller;

import com.test.Echojorgesidgo.exception.ExistingClientException;
import com.test.Echojorgesidgo.model.dto.ClienteDTO;
import com.test.Echojorgesidgo.model.dto.CuentaDTO;
import com.test.Echojorgesidgo.model.service.ClienteService;
import com.test.Echojorgesidgo.model.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    CuentaService cuentaService;

    // Views

    @GetMapping("/agregar")
    public String addClientView(
            Model model
    ) {
        model.addAttribute("cliente", new ClienteDTO());
        model.addAttribute("errors", new HashMap<String, String>());
        return "new-client";
    }

    @GetMapping
    public String clientView(
            Model model
    ) {

        model.addAttribute("clientes", clienteService.list());

        return "cliente";
    }

    @GetMapping("/editar/{id}")
    public String editarCliente(
            @PathVariable("id") long id,
            Model model
    ) {

        model.addAttribute("cliente", clienteService.getOne(id));
        model.addAttribute("errors", new HashMap<String, String>());

        return "editar-client";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarCliente(
            @PathVariable("id") long id,
            Model model
    ) {

        model.addAttribute("cliente", clienteService.getOne(id));
        model.addAttribute("errors", new HashMap<String, String>());

        return "eliminar-client";
    }

    @GetMapping("/cuentas/{id}")
    public String cuentasCliente(
            @PathVariable("id") long id,
            Model model
    ) {

        model.addAttribute("cuentas", cuentaService.cuentasCliente(id));
        model.addAttribute("cliente", clienteService.getOne(id));
        model.addAttribute("errors", new HashMap<String, String>());

        return "cuentas-client";
    }

    @GetMapping("/agregar-cuenta/{id}")
    public String agregarCuentasCliente(
            @PathVariable("id") long id,
            Model model
    ) {

        model.addAttribute("cuenta", new CuentaDTO());
        model.addAttribute("cliente", clienteService.getOne(id));
        model.addAttribute("errors", new HashMap<String, String>());


        return "agregar-cuentas-client";
    }

    // Methods

    @PostMapping
    public String addCliente(
            @Valid ClienteDTO cliente,
            BindingResult result,
            Model model
    ) throws ExistingClientException {
        ClienteDTO clienteDTO = new ClienteDTO();
        Map<String, String> errors = new HashMap<>();
        try {

            result.getFieldErrors().forEach(item -> errors.put(item.getField(), item.getDefaultMessage()));

            if (result.hasErrors()) {
                model.addAttribute("cliente", cliente);
                model.addAttribute("errors", errors);
                return "new-client";
            }

            clienteDTO = clienteService.save(cliente);

            model.addAttribute("cliente", new ClienteDTO());
            model.addAttribute("newCliente", clienteDTO);

        }
        catch (ExistingClientException ex) {
            errors.put("dui", "Ya existe un cliente con este DUI");
            model.addAttribute("cliente", cliente);
            model.addAttribute("errors", errors);
            return "new-client";
        }

        catch (Exception ex) {
            throw ex;
        }

        return "redirect:/clientes";
    }

    @PostMapping("/editar/{id}")
    public String editCliente(
            @Valid @ModelAttribute ClienteDTO cliente,
            @PathVariable("id") long id,
            BindingResult result,
            Model model
    ) throws ExistingClientException {
        ClienteDTO clienteDTO = new ClienteDTO();
        Map<String, String> errors = new HashMap<>();
        try {

            cliente.setCodCliente(id);
            result.getFieldErrors().forEach(item -> errors.put(item.getField(), item.getDefaultMessage()));

            if (result.hasErrors()) {
                model.addAttribute("cliente", cliente);
                model.addAttribute("errors", errors);
                return "editar-client";
            }

            clienteDTO = clienteService.update(id, cliente);

            model.addAttribute("cliente", new ClienteDTO());
            model.addAttribute("newCliente", clienteDTO);

        }
        catch (ExistingClientException ex) {
            errors.put("dui", "Ya existe un cliente con este DUI");
            model.addAttribute("cliente", cliente);
            model.addAttribute("errors", errors);
            return "editar-client";
        }
        catch (Exception ex) {
            throw ex;
        }

        return "redirect:/clientes";
    }

    @PostMapping("/agregar-cuenta/{id}")
    public String agregarCuenta(
            @Valid CuentaDTO cuenta,
            @PathVariable("id") long id,
            BindingResult result,
            Model model
    ) {
        CuentaDTO cuentaDTO = new CuentaDTO();

        try {

            Map<String, String> errors = new HashMap<>();

            result.getFieldErrors().forEach(item -> errors.put(item.getField(), item.getDefaultMessage()));

            if (result.hasErrors()) {
                model.addAttribute("cuenta",cuenta);
                model.addAttribute("cliente", clienteService.getOne(id));
                model.addAttribute("errors", errors);
                return "agregar-cuentas-client";
            }


            cuentaDTO = cuentaService.agregarCuenta(id, cuenta);



        } catch (Exception ex) {
            throw ex;
        }

        return "redirect:/clientes/cuentas/" + id;
    }

    @PostMapping("/delete/{id}")
    public String deleteClient(
            @PathVariable("id") long id,
            Model model
    ) {
        clienteService.delete(id);
        return "redirect:/clientes";
    }

}
