package com.test.Echojorgesidgo.model.repository;

import com.test.Echojorgesidgo.model.dto.ClienteDTO;
import com.test.Echojorgesidgo.model.entity.Cliente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends BaseRepository<Cliente, Long> {

    Cliente getClienteByDui(String dui);

}
