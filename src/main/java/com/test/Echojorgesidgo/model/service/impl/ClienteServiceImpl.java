package com.test.Echojorgesidgo.model.service.impl;

import com.test.Echojorgesidgo.exception.ExistingClientException;
import com.test.Echojorgesidgo.model.dto.ClienteDTO;
import com.test.Echojorgesidgo.model.dto.CuentaDTO;
import com.test.Echojorgesidgo.model.entity.Cliente;
import com.test.Echojorgesidgo.model.entity.Cuenta;
import com.test.Echojorgesidgo.model.repository.ClienteRepository;
import com.test.Echojorgesidgo.model.repository.CuentaRepository;
import com.test.Echojorgesidgo.model.service.ClienteService;
import com.test.Echojorgesidgo.model.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    CuentaService cuentaService;


    @Override
    public List<ClienteDTO> list() {
        List<Cliente> clientes;

        clientes = clienteRepository.findAll();

        return clientes.stream().map(ClienteDTO::fromEntity).collect(Collectors.toList());
    }

    @Override
    public ClienteDTO save(ClienteDTO clienteDTO) throws ExistingClientException {
        Cliente cliente = new Cliente();
        ClienteDTO finalDTO = new ClienteDTO();

        try {

            if (clienteRepository.getClienteByDui(clienteDTO.getDui()) != null)
                throw new ExistingClientException("El cliente ya existe");

            cliente.setNombre(clienteDTO.getNombre());
            cliente.setApellido(clienteDTO.getApellido());
            cliente.setDui(clienteDTO.getDui());

            finalDTO = ClienteDTO.fromEntity(clienteRepository.save(cliente));

            finalDTO.setCuentas(cuentaService.crearCuentas(finalDTO.getCodCliente(), clienteDTO.getCuentas()));

        } catch (ExistingClientException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }

        return finalDTO;
    }

    @Override
    public ClienteDTO update(long id, ClienteDTO clienteDTO) throws ExistingClientException {
        Cliente cliente = new Cliente();
        ClienteDTO finalDTO = new ClienteDTO();

        try {

            cliente = clienteRepository.getOne(id);

            if (clienteRepository.getClienteByDui(clienteDTO.getDui()) != null && (!cliente.getDui().equals(clienteDTO.getDui())) )
                throw new ExistingClientException("El cliente ya existe");


            cliente.setNombre(clienteDTO.getNombre());
            cliente.setApellido(clienteDTO.getApellido());
            cliente.setDui(clienteDTO.getDui());

            finalDTO = ClienteDTO.fromEntity(clienteRepository.save(cliente));

        } catch (ExistingClientException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }

        return finalDTO;
    }

    @Override
    public ClienteDTO getOne(long id) {
        return ClienteDTO.fromEntity(clienteRepository.getOne(id));
    }

    @Override
    public void delete(long idCliente) {
        clienteRepository.deleteById(idCliente);
    }
}
