package com.test.Echojorgesidgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EchojorgesidgoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EchojorgesidgoApplication.class, args);
	}

}
