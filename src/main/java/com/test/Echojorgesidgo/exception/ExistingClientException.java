package com.test.Echojorgesidgo.exception;

public class ExistingClientException extends Exception {

    public ExistingClientException(String message) {
        super(message);
    }

}
