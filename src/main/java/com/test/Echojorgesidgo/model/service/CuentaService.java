package com.test.Echojorgesidgo.model.service;

import com.test.Echojorgesidgo.model.dto.CuentaDTO;

import java.math.BigDecimal;
import java.util.List;

public interface CuentaService {

    CuentaDTO deposito(BigDecimal monto);
    List<CuentaDTO> crearCuentas(long idCliente, List<CuentaDTO> cuentas);
    CuentaDTO agregarCuenta(long idCliente, CuentaDTO cuentaDTO);
    CuentaDTO getCuenta(String numCuenta);
    CuentaDTO deposito(CuentaDTO cuentaDTO);
    List<CuentaDTO> cuentasCliente(long idCliente);

}
